# README #

>this repo is for huge load testing task.

>use docker swarm mode achieve distributed load testing.

>[Tutorial ref](http://www.testautomationguru.com/jmeter-scaling-out-load-servers-using-docker-compose-in-distributed-load-testing/)

### How do I get set up? ###

* docker stack deploy -c docker-compose.yml jmeter
* docker exec -it "MASTER's CONTAINER ID" /bin/bash (into master node run non-gui jmeter)

### Visualize data (.jtl)###

[Here is good online tools](https://sense.blazemeter.com/gui/)